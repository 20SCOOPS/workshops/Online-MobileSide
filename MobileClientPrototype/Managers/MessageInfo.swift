//
//  MessageInfo.swift
//  ServerActingPrototype
//
//  Created by Dhanu on 26/6/2561 BE.
//  Copyright © 2561 20scoops. All rights reserved.
//

import Foundation

struct TextMessageInfo : Codable {
    var id : Int
    var sender : String
    var message : String
}

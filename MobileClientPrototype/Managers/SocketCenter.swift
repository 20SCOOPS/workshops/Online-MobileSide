import PubNub
class SocketCenter : NSObject,  PNObjectEventListener, Singleton {
    let configuration = PNConfiguration(
        publishKey: "pub-5fe1a4b0-caaf-454a-9707-9341717f7f0e",
        subscribeKey: "sub-2dc42151-e0f0-11e0-96c6-750c0515ff54"
    )
    lazy var client = PubNub.clientWithConfiguration(configuration)
    
    var channels : [String:(Data)->()] = [:]
    
    override required init() {
        super.init()
        configuration.stripMobilePayload = false
        client.addListener(self)
    }
    func removeChannel(name:String...) {
        client.unsubscribeFromChannels(name, withPresence: false)
        name.forEach { self.channels[$0] = nil }
    }
    func addChanel(name:String..., observer:@escaping (Data)->()) {
        name.forEach { self.channels[$0] = observer }
        client.subscribeToChannels(name, withPresence: false)
    }
    func publish(_ message:Data, to channel:String, with completion: ((Bool)->())? = nil ) {
        guard let data = try? JSONSerialization.jsonObject(with: message, options: .allowFragments) else { return }
        client.publish(data, toChannel: channel) { status in
            switch status.category {
            case .PNAcknowledgmentCategory: completion?(true)
            default: completion?(false)
            }
        }
    }
    func client(_ client: PubNub, didReceiveMessage message: PNMessageResult) {
        if let string = message.data.message as? String,
            let data = string.data(using: .utf8) {
            channels[message.data.channel]?(data)
            return
        }
        guard let messageData = message.data.message as? [String:Any],
            let data = try? JSONSerialization.data(withJSONObject: messageData, options: .prettyPrinted)
            else { return }
        channels[message.data.channel]?(data)
    }
}

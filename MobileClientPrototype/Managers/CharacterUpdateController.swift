struct Vec3 : Codable {
    var x : Float
    var y : Float
    var z : Float
}
struct CharacterTransform : Codable {
    var pos : Vec3
    var rot : Vec3
}
struct CharacterTransformObject : Codable {
    var id : String
    var transform : CharacterTransform
}
struct CharacterUpdateObject : Codable {
    var id : String
    var name : String
    var transform : CharacterTransform
    var style : CharacterStyle
    var transformObject : CharacterTransformObject {
        return CharacterTransformObject(id: id, transform: transform)
    }
}

class CharacterUpdateController : SingletonObject {
    var channelT = "updateT"
    var channel = "update"

    func observe(handler: @escaping (CharacterTransformObject)->()) {
        SocketCenter.shared.addChanel(name: channelT) { data in
            guard let object = try? JSONCoder.shared.decode(CharacterTransformObject.self, from: data) else { return }
            handler(object)
        }
    }
    func observe(handler: @escaping (CharacterUpdateObject)->()) {
        SocketCenter.shared.addChanel(name: channel) { data in
            guard let object = try? JSONCoder.shared.decode(CharacterUpdateObject.self, from: data) else { return }
            handler(object)
        }
    }
    func update(_ object:CharacterUpdateObject) {
        guard let data = try? JSONCoder.shared.encode(object) else { return }
        SocketCenter.shared.publish(data, to: channel)
    }
    func update(_ object:CharacterTransformObject) {
        guard let data = try? JSONCoder.shared.encode(object) else { return }
        SocketCenter.shared.publish(data, to: channelT)
    }
    func clearObservers() {
        SocketCenter.shared.removeChannel(name: channelT)
        SocketCenter.shared.removeChannel(name: channel)
    }
}

import UIKit
class SingletonObject : Singleton {
    required init() {}
}
protocol CharacterStyleProtocol: Codable {
    var body : Int {get}
    var eyes : Int {get}
    var mouth : Int {get}
}
struct CharacterStyle : CharacterStyleProtocol {
    var body : Int
    var eyes : Int
    var mouth : Int
    static var `default` = CharacterStyle(body: 1, eyes: 1, mouth: 1)
}
class PlayerModel : SingletonObject {
    lazy var object = CharacterUpdateObject (
        id : UIDevice.current.identifierForVendor?.uuidString ?? "none",
        name : "Player",
        transform : CharacterTransform(
            pos : Vec3(x: 0, y: 0, z: 0),
            rot : Vec3(x: 0, y: 0, z: 0)
        ),
        style : CharacterStyle(body: 1, eyes: 1, mouth: 1)
    )
}

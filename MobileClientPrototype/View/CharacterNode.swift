import SceneKit

class CharacterNode : SCNNode {
    var id : String
    var lastUpdated : Date
    var char : SCNNode
    var body : SCNNode?
    var eyes : SCNNode?
    var mouth : SCNNode?
    
    init?(_ id:String, style:CharacterStyleProtocol = CharacterStyle.default) {
        guard
            let scene = SCNScene(named: "art.scnassets/char.scn"),
            let char = scene.rootNode.childNode(withName: "char", recursively: true)
        else { return nil }
        self.char = char
        
        self.id = id
        self.lastUpdated = Date()
        
        super.init()
        addChildNode(char)
        
        body = char.childNode(withName: "body", recursively: true)
        eyes = char.childNode(withName: "eyes", recursively: true)
        mouth = char.childNode(withName: "mouth", recursively: true)
        
        setStyle(character: style)
    }
    func setStyle(character:CharacterStyleProtocol) {
        setBodyColor(number: character.body)
        setEyes(number: character.eyes)
        setMouth(number: character.mouth)
    }
    func setBodyColor(number:Int) {
        guard let materials = body?.geometry?.materials else { return }
        let name = "body\(number)"
        body?.geometry?.materials = materials.sorted { (mat1,mat2)  in
            return mat1.name == name
        }
    }
    func setEyes(number:Int) {
        guard let materials = eyes?.geometry?.materials else { return }
        let name = "eyes\(number)"
        eyes?.geometry?.materials = materials.sorted { (mat1,mat2)  in
            return mat1.name == name
        } 
    }
    func setMouth(number:Int) {
        guard let materials = mouth?.geometry?.materials else { return }
        let name = "mouth\(number)"
        mouth?.geometry?.materials = materials.sorted { (mat1,mat2)  in
            return mat1.name == name
        }
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

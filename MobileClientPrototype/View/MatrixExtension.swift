import ARKit
import SceneKit

extension matrix_float4x4 {
    var pos : Vec3 {
        return Vec3(x: columns.3.x, y: columns.3.y, z: columns.3.z)
    }
    var rot : Vec3 {
        let y = atan2(columns.1.z, columns.2.z)
        let c2 = sqrt(pow(columns.0.x, 2) + pow(columns.0.y, 2))
        let x = atan2(-columns.0.z, c2)
        return Vec3(x:-x, y:.pi - y,z: 0)
    }
}

extension Vec3 {
    var toSCN : SCNVector3 { return SCNVector3Make(x, y, z) }
}

//
//  ViewController.swift
//  MobileClientPrototype
//
//  Created by Dhanu on 27/6/2561 BE.
//  Copyright © 2561 20scoops. All rights reserved.
//

import UIKit
import SceneKit
import ARKit

class ViewController: UIViewController, ARSCNViewDelegate, ARSessionDelegate {
    var timeInterval = 0.3
    @IBOutlet var sceneView: ARSCNView!
    var players : [CharacterNode] = []
    var timer : Timer?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set the view's delegate
        sceneView.delegate = self
        
        // Show statistics such as fps and timing information
//        sceneView.showsStatistics = true
        
        // Create a new scene
        guard let scene = SCNScene(named: "art.scnassets/stage.scn") else { return }
        
        // Set the scene to the view
        sceneView.scene = scene
        let enemy = scene.rootNode.childNode(withName: "enemy", recursively: true)
        let source = SCNAudioSource(named: "fireplace.mp3")!
        source.loops = true
        let player = SCNAudioPlayer(source: source)
        enemy?.addAudioPlayer(player)
        
    }
    func add(player : CharacterUpdateObject) {
        guard let p = CharacterNode(player.id, style: player.style) else { return }
        sceneView.scene.rootNode.addChildNode(p)
        players.append(p)
        update(player: p, transform: player.transform)
    }
    func update(player:CharacterNode, with: CharacterUpdateObject) {
        update(player: player, transform: with.transform)
        update(player: player, with: with.style)
    }
    func update(player:CharacterNode, with: CharacterStyle) {
        player.setStyle(character: with)
    }
    func update(player: CharacterNode, transform: CharacterTransform ) {
        player.lastUpdated = Date()
        SCNTransaction.begin()
        SCNTransaction.animationDuration = timeInterval
        player.position = transform.pos.toSCN
        player.eulerAngles = transform.rot.toSCN
        SCNTransaction.commit()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Create a session configuration
        let configuration = ARWorldTrackingConfiguration()
        configuration.worldAlignment = .gravityAndHeading
        // Run the view's session
        sceneView.session.run(configuration, options: [.resetTracking, .removeExistingAnchors])
        sceneView.session.delegate = self
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // start update self to channel
        settingUpdateObserving()
        settingTimer()
        Promise.delay(1, PlayerModel.shared.object).then {
            CharacterUpdateController.shared.update($0)
        }
    }
    func find(player id:String) -> CharacterNode? {
        return self.players.filter { return $0.id == id }.first
    }
    func cleanTimeoutPlayer() {
        players = players.filter {
            let stay = $0.lastUpdated.timeIntervalSinceNow < 5
            if !stay { $0.removeFromParentNode() }
            return stay
        }
    }
    func settingUpdateObserving() {
        CharacterUpdateController.shared.observe { [weak self] (character:CharacterUpdateObject) in
            guard character.id != PlayerModel.shared.object.id else { return }
            let player = self?.find(player: character.id)
            if let p = player { self?.update(player: p, with: character) }
            else {
                //someone new here update back to him
                self?.add(player: character)
                CharacterUpdateController.shared.update(PlayerModel.shared.object)
            }
        }
        CharacterUpdateController.shared.observe { [weak self]  (transform:CharacterTransformObject) in
            let player = self?.find(player: transform.id)
            if let p = player { self?.update(player: p, transform: transform.transform) }
        }
    }
    func settingTimer() {
        timer = Timer.scheduledTimer(withTimeInterval: timeInterval, repeats: true) { [weak self] (timer) in
            CharacterUpdateController.shared.update(PlayerModel.shared.object.transformObject)
            self?.cleanTimeoutPlayer()
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
        CharacterUpdateController.shared.clearObservers()
        timer?.invalidate()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }
    // MARK: - ARSCNViewDelegate
    
    func session(_ session: ARSession, didUpdate frame: ARFrame) {
        let transform = frame.camera.transform
        PlayerModel.shared.object.transform.pos = transform.pos
        PlayerModel.shared.object.transform.rot = transform.rot
    }
/*
    // Override to create and configure nodes for anchors added to the view's session.
    func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
        let node = SCNNode()
     
        return node
    }
*/
    func session(_ session: ARSession, didFailWithError error: Error) {
        // Present an error message to the user
        
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        // Inform the user that the session has been interrupted, for example, by presenting an overlay
        
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        // Reset tracking and/or remove existing anchors if consistent tracking is required
        
    }
}

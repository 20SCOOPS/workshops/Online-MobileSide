//
//  CharacterSettingViewController.swift
//  MobileClientPrototype
//
//  Created by Dhanu on 6/7/2561 BE.
//  Copyright © 2561 20scoops. All rights reserved.
//

import UIKit
import SceneKit

class CharacterSettingViewController: UIViewController {
    @IBOutlet var sceneView : SCNView!
    @IBOutlet var nameField : UITextField!
    var character : CharacterNode!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        character = CharacterNode(PlayerModel.shared.object.id)
        guard let scene = SCNScene(named: "art.scnassets/char.scn") else { return }
        sceneView.scene = scene
        scene.rootNode.childNode(withName: "char", recursively: true)?.removeFromParentNode()
        scene.rootNode.addChildNode(character)
        character.position = SCNVector3Make(0, 0, 0)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func settingBody(_ sender:UISegmentedControl) {
        PlayerModel.shared.object.style.body = sender.selectedSegmentIndex + 1
        character.setStyle(character: PlayerModel.shared.object.style)
    }
    @IBAction func settingMouth(_ sender:UISegmentedControl) {
        PlayerModel.shared.object.style.mouth = sender.selectedSegmentIndex + 1
        character.setStyle(character: PlayerModel.shared.object.style)
    }
    @IBAction func settingEyes(_ sender:UISegmentedControl) {
        PlayerModel.shared.object.style.eyes = sender.selectedSegmentIndex + 1
        character.setStyle(character: PlayerModel.shared.object.style)
    }
    @IBAction func submit() {
        performSegue(withIdentifier: "start", sender: nil)
    }
    @IBAction func touch(_ gesture:UIPanGestureRecognizer) {
        if gesture.state == .changed {
            let screenPoint = gesture.location(in: sceneView)
            var pos = sceneView.unprojectPoint(SCNVector3Make(Float(screenPoint.x), Float(screenPoint.y), 1))
            pos.x *= -1
            pos.y *= -1
            character.eulerAngles = SCNVector3Make(0, 0, 0)
            character.look(at: pos)
        }
        else {
            character.eulerAngles = SCNVector3Make(0, 0, 0)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func unwindToSetting(segue:UIStoryboardSegue) {}
}

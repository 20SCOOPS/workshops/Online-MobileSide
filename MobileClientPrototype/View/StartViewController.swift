import UIKit

class StartViewController : UIViewController {
    @IBOutlet var nameField : UITextField!
    @IBOutlet var textField : UITextField!
    @IBOutlet var logView : UITextView!
    var count = 0
    var log : String = "" {
        didSet { logView.text = log }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        log = "Welcome!"
        nameField.text = PlayerModel.shared.object.name
        nameField.placeholder = "Your name"
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        SocketCenter.shared.addChanel(name: "message") {[weak self] data in
            guard let textMessage = try? JSONCoder.shared.decode(TextMessageInfo.self, from: data)
                else { return }
            self?.log = (self?.log ?? "") + "\n" + textMessage.sender + " : " + textMessage.message
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        SocketCenter.shared.removeChannel(name: "message")
    }
    @IBAction func send(_ sender : Any) {
        guard let name = nameField.text,
            let text = textField.text,
            name.count > 0,
            text.count > 0 else {
            requiedNameAlert()
            return
        }
        count += 1
        let textMessage = TextMessageInfo(id: count, sender: name, message: text)
        guard let data = try? JSONCoder.shared.encode(textMessage) else { return }
        SocketCenter.shared.publish(data, to: "message")
        textField.text = ""
    }
    func requiedNameAlert() {
        let alert = UIAlertController(title: "Require Name and Text", message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}
extension StartViewController : UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        PlayerModel.shared.object.name = textField.text ?? "Anonymous"
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.textField {
            send(textField)
        }
        else if textField == nameField {
            self.textField.becomeFirstResponder()
        }
        return true
    }
}

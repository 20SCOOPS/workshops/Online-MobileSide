//
//  Regex.swift
//  Playground
//
//  Created by Dhanu on 17/4/2561 BE.
//  Copyright © 2561 20scoops. All rights reserved.
//

import Foundation

class Regex {
    let expression:NSRegularExpression
    let pattern: String
    init(_ pattern:String, _ options:NSRegularExpression.Options = .caseInsensitive) throws {
        self.pattern = pattern
        self.expression = try NSRegularExpression(pattern: pattern, options: options)
    }
    func test(input: String) -> Bool {
        return expression.matches(in: input, range: NSMakeRange(0, input.count)).count > 0
    }
}

extension String {
    public static let emailRegexPattern = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
    func verifiedEmail()->Bool {
        do {
            return try Regex(.emailRegexPattern).test(input:self)
        }
        catch { debugPrint("cant do verifiedEmail Regex:",error) }
        return false
    }
}

import Foundation

extension String {
    func snakeCased() -> String? {
        return splitCamelCase(with: "_")
    }
    func snakeDashCased() -> String? {
        return splitCamelCase(with: "-")
    }
    func splitCamelCase(with character:Character) -> String? {
        return (try? NSRegularExpression(
            pattern: "([a-z0-9])([A-Z])",
            options: [])
            )?.stringByReplacingMatches(
                in: self,
                options: [],
                range: NSRange(location: 0, length: self.count),
                withTemplate: "$1\(character)$2"
            ).lowercased()
    }
    func format(arguments: CVarArg...) -> String {
        return self.format(arguments: arguments)
    }
    func format(arguments: [CVarArg]) -> String {
        return String(format: self, arguments: arguments)
    }
    public func append(string:String, in range:NSRange)->String {
        if let range = Range(range,in:self) {
            return self.replacingCharacters(in:range , with: string)
        } else { return self+string }
    }
}

extension StringProtocol {
    public var stringValue : String {
        return String(self)
    }
}

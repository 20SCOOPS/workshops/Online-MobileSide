//
//  ServiceExtensionSender.swift
//  Playground
//
//  Created by Dhanu on 18/4/2561 BE.
//  Copyright © 2561 20scoops. All rights reserved.
//

import Foundation

public extension Promise where ReturnType : ServiceProtocol {
    fileprivate func thenSend<T>(closure:@escaping (URLRequest,ReturnType.Rte)->Promise<T>)->Promise<T> {
        return then { service->Promise<T> in
            guard let request = service.request.urlRequest else { throw ErrorMessage(message:"no_request") }
            return closure(request,service.route)
        }
    }
    @discardableResult public func send<ResponseType:Codable>()->Promise<ResponseType> {
        return thenSend { return $1.send(request: $0) }
    }
    @discardableResult public func send()->Promise<[String:Any]> {
        return thenSend { return $1.send(request: $0) }
    }
    @discardableResult public func send()->Promise<Data> {
        return thenSend { return $1.send(request: $0) }
    }
}

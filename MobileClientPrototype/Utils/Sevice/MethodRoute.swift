//
//  MethodRoute.swift
//  Playground
//
//  Created by Dhanu on 19/4/2561 BE.
//  Copyright © 2561 20scoops. All rights reserved.
//

import Foundation

public protocol Get {}
public protocol Post {}
public protocol Put {}
public protocol Patch {}
public protocol Delete {}
public protocol Head {}
public protocol Options {}
public protocol Connect {}
public protocol Trace {}

public typealias Creatable = Get&Post
public typealias Updatable = Get&Put
public typealias Managable = Creatable&Put
public typealias FullManagable = Managable&Delete
public typealias AllMethod = FullManagable&Patch&Head&Options&Connect&Trace

extension Service where Rte : Get {
    static public func get()->Promise<Service> { return use(method: #function.asHttpMethod) }
}
extension Service where Rte : Post {
    static public func post()->Promise<Service> { return use(method: #function.asHttpMethod) }
}
extension Service where Rte : Put {
    static public func put()->Promise<Service> { return use(method: #function.asHttpMethod) }
}
extension Service where Rte : Patch {
    static public func patch()->Promise<Service> { return use(method: #function.asHttpMethod) }
}
extension Service where Rte : Delete {
    static public func delete()->Promise<Service> { return use(method: #function.asHttpMethod) }
}
extension Service where Rte : Head {
    static public func head()->Promise<Service> { return use(method: #function.asHttpMethod) }
}
extension Service where Rte : Options {
    static public func options()->Promise<Service> { return use(method: #function.asHttpMethod) }
}
extension Service where Rte : Connect {
    static public func connect()->Promise<Service> { return use(method: #function.asHttpMethod) }
}
extension Service where Rte : Trace {
    static public func trace()->Promise<Service> { return use(method: #function.asHttpMethod) }
}

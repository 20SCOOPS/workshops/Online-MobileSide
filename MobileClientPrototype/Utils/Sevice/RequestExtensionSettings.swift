//
//  RequestExtensionSettings.swift
//  Playground
//
//  Created by Dhanu on 18/4/2561 BE.
//  Copyright © 2561 20scoops. All rights reserved.
//

import Foundation

extension ServiceProtocol {
    func mutate(closure:@escaping (inout Self)->Void)->Self {
        var copy = self
        closure(&copy)
        return copy
    }
}

public extension Promise where ReturnType : ServiceProtocol {
    public func set(query:ReturnType.Req.Query)->Promise<ReturnType> {
        return then { return $0.mutate { $0.request.query = query } }
    }
    public func set(body:ReturnType.Req.Body)->Promise<ReturnType> {
        return then { return $0.mutate { $0.request.body = body } }
    }
    public func set(cachePolicy:URLRequest.CachePolicy)->Promise<ReturnType> {
        return then { return $0.mutate { $0.request.cachePolicy = cachePolicy } }
    }
    public func set(timeout:TimeInterval)->Promise<ReturnType> {
        return then { return $0.mutate { $0.request.timeout = timeout } }
    }
    public func add(header:String, value:String)->Promise<ReturnType> {
        return then { return $0.mutate { $0.request.header[header] = value } }
    }
    public func add(component:String)->Promise<ReturnType> {
        return then { return $0.mutate { $0.request.components.append(component) } }
    }
    public func addContentTypeJson()->Promise<ReturnType> {
        return add(contentType: "application/json")
    }
    public func addContentTypeURLEncode()->Promise<ReturnType> {
        return add(contentType: "application/x-www-form-urlencoded")
    }
    public func add(contentType:String)->Promise<ReturnType> {
        return add(header: "Content-Type", value: contentType)
    }
    public func add(authorization:String)->Promise<ReturnType> {
        return add(header: "Authorization", value: authorization)
    }
}

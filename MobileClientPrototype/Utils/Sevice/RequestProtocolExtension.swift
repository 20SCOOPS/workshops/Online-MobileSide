//
//  RequestProtocolExtension.swift
//  Playground
//
//  Created by Dhanu on 18/4/2561 BE.
//  Copyright © 2561 20scoops. All rights reserved.
//

import Foundation

extension RequestProtocol {
    var fullURL : URL? {
        guard var componentsURL = URLComponents(url: url, resolvingAgainstBaseURL: true) else {
            return nil
        }
        components.forEach {
            componentsURL.path += "/\($0)"
        }
        if let queryItems = queryItems {
            componentsURL.queryItems = (componentsURL.queryItems ?? []) + queryItems
        }
        return componentsURL.url
    }
    var urlRequest : URLRequest? {
        guard let fullURL = fullURL else { return nil }
        debugPrint("create request:",fullURL.absoluteString)
        var request = URLRequest(url: fullURL, cachePolicy: cachePolicy, timeoutInterval: timeout)
        request.httpBody = httpBody
        request.httpMethod = method
        request.allHTTPHeaderFields = header
        return request
    }
}

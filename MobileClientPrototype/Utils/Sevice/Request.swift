//
//  Request.swift
//  Playground
//
//  Created by Dhanu on 18/4/2561 BE.
//  Copyright © 2561 20scoops. All rights reserved.
//

import Foundation

let DEFAULT_REQUEST_TIME_OUT_SEC = 30

struct Unused : Codable {}

class Request<Body:Codable, Query:Codable> {
    var url : URL
    var method : String = "get"
    var query : Query?
    var body : Body?
    var components = [String]()
    var header = [String : String]()
    
    var cachePolicy = URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData
    var timeout = TimeInterval(DEFAULT_REQUEST_TIME_OUT_SEC)
    
    required init(_ url:URL,method:String) {
        self.url = url
        self.method = method
    }
}

extension Request  : RequestProtocol {
    var httpBody : Data? {
        return try? JSONCoder.shared.encode(body)
    }
    var queryItems : [URLQueryItem]? {
        guard let query = query else { return nil }
        return try? JSONCoder.shared.queryItems(query)
    }
}

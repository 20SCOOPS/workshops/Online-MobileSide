//
//  Service.swift
//  Playground
//
//  Created by Dhanu on 18/4/2561 BE.
//  Copyright © 2561 20scoops. All rights reserved.
//

import Foundation

extension String {
    var asHttpMethod : String {
        return self.split(separator: "(").first!.uppercased()
    }
}

public protocol ServiceProtocol {
    associatedtype Req:RequestProtocol
    associatedtype Rte:RouteProtocol
    var request : Req { get set }
    var route : Rte { get set }
}

public struct Service<Req:RequestProtocol, Rte:RouteProtocol> : ServiceProtocol{
    public var request : Req
    public var route : Rte
}
public extension Service {
    public static func use(method:String = "GET", url:URL? = nil)->Promise<Service>{
        let route = Rte()
        let request = Req(url ?? route.path, method:method)
        return Promise(resolve: Service(request:request,route:route))
    }
}

//
//  RequestProtocol.swift
//  Playground
//
//  Created by Dhanu on 18/4/2561 BE.
//  Copyright © 2561 20scoops. All rights reserved.
//

import Foundation

public protocol RequestProtocol {
    associatedtype Body
    associatedtype Query
    // base url
    var method : String { get }
    var url : URL  { get }
    var query : Query? { get set }
    var body : Body? { get set }
    
    // how request
    var cachePolicy : URLRequest.CachePolicy { get set}
    var timeout : TimeInterval { get set}
    // what input
    var components : [String] { get set }
    var header : [String : String] { get set }
    // parse query to URLQueryItems
    var queryItems : [URLQueryItem]? { get }
    // parse body to Data
    var httpBody : Data? { get }
    
    // full url with components & query
    var fullURL : URL? { get }
    // complete request
    var urlRequest : URLRequest? { get }
    
    init(_ url:URL, method:String)
}

import Foundation


extension String {
    static let dataBoundary = "----20ScoopsCNX"
}

extension Data {
    public mutating func append(_ string: String ,encoding: String.Encoding = .utf8) {
        guard let data = string.data(using: encoding) else { return }
        append(data)
    }
}

public extension Promise where ReturnType : ServiceProtocol, ReturnType.Req.Body == Data, ReturnType.Rte : Post {
    
    public func add(image:Data, name:String="image")->Promise<ReturnType> {
        return add(image:image, name:name, parameters:Unused())
    }
    public func add<T:Codable>(image:Data, name:String="image", parameters:T?)->Promise<ReturnType> {
        return add(file: image, name: name, mimetype: "image/png", parameters: parameters)
    }
    
    public func add<T:Codable>(file:Data, name:String, mimetype:String, parameters:T?)->Promise<ReturnType> {
        let params: [URLQueryItem]? = T.self == Unused.self ? nil : (try? JSONCoder.shared.queryItems(parameters))
        return add(contentType: "multipart/form-data; boundary=\(String.dataBoundary)")
            .set(body:createBody(
            parameters:params,
            filePathKey: name,
            dataKey:file,
            mimetype: mimetype
        ))
    }
    func param(with boundary:String, key:String, value:String?)->String? {
        guard let value = value else { return nil }
        return "--\(String.dataBoundary)\r\n" +
            "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n" +
        "\(value)\r\n"
    }
    func createBody(
        parameters: [URLQueryItem]?,
        filePathKey: String,
        dataKey: Data,
        mimetype:String) -> Data {
        var body = Data()
        if let params = parameters {
            body.append(
                params.compactMap {
                    return self.param(with: .dataBoundary, key: $0.name, value: $0.value)
                    }.joined()
            )
        }
        let filename = "img-\(filePathKey.lowercased()).png"
        body.append(
            "--\(String.dataBoundary)\r\n" +
            "Content-Disposition: form-data; name=\"\(filePathKey)\"; filename=\"\(filename)\"\r\n" +
            "Content-Type: \(mimetype)\r\n\r\n"
        )
        
        debugPrint("size",dataKey.count)
        body.append(dataKey)
        body.append("\r\n--\(String.dataBoundary)--\r\n")
        return body
    }
    
}

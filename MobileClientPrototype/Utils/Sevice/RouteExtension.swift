//
//  RouteExtension.swift
//  Playground
//
//  Created by Dhanu on 19/4/2561 BE.
//  Copyright © 2561 20scoops. All rights reserved.
//

import Foundation

public extension APIRoute {
    fileprivate func verify(data:Data?)throws -> Data {
        if let data = data { return data }
        else { throw ErrorMessage(message: "no_data") }
    }
    fileprivate func verifyError(from response:URLResponse?, with data:Data?) throws {
        if let res = response as? HTTPURLResponse ,
            res.statusCode != 200 {
            throw ErrorResponse(
                statusCode:res.statusCode,
                data:data ?? ErrorData(message:"Unknown Error \(res.statusCode)", messages:[]).asData!
            )
        }
    }
    @discardableResult public func send<ResponseType:Codable>(request:URLRequest)->Promise<ResponseType> {
        return send(request:request).then { (data:Data) in
            return try JSONCoder.shared.decode(ResponseType.self, from: data)
        }
    }
    @discardableResult public func send(request:URLRequest)->Promise<[String:Any]> {
        return send(request:request).then { (data:Data) in
            return try JSONCoder.shared.keyValues(data)
        }
    }
    @discardableResult public func send(request:URLRequest)->Promise<Data> {
        return Promise<Data> { (rs, rj) in
            URLSession.shared.dataTask(with: request) { (dat,res,err) in
//                if let data = dat,
//                    let string = String(data:data, encoding:.utf8) {
//                    print(string)
//                }
                do {
                    try self.verifyError(from: res, with: dat)
                    if let error = err { throw error }
                    rs(try self.verify(data: dat))
                }
                catch {
                    rj(error)
                }
            }.resume()
        }
    }
}

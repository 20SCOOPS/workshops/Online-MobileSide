//
//  Route.swift
//  ICA
//
//  Created by Dhanu on 10/4/2561 BE.
//  Copyright © 2561 Akarapas Wongkaew. All rights reserved.
//

import Foundation
    
public protocol RouteProtocol {
    var path:URL { get }
    init()
    @discardableResult func send<ResponseType:Codable>(request:URLRequest)->Promise<ResponseType>
    @discardableResult func send(request:URLRequest)->Promise<[String:Any]>
    @discardableResult func send(request:URLRequest)->Promise<Data>
}

public protocol APIRoute : RouteProtocol {}
    
public class NodeRoute<Front : RouteProtocol> : APIRoute {
    public required init() {}
    public var name:String? {
        var className = String(describing: type(of: self)).split(separator: "<").first!
        if className.hasSuffix("Route") {
            className = className.prefix(className.count-5)
        }
        return String(className).snakeDashCased()
    }
    public var path:URL {
        var base = Front().path
        if let name = name {
            base.appendPathComponent(name)
        }
        return  base
    }
}

import Foundation

extension Int64 {
    var asDate : Date { return Date(timeIntervalSince1970: TimeInterval(self)) }
}

extension DateFormatter {
    convenience init(dateFormat:String) {
        self.init()
        self.dateFormat = dateFormat
        self.calendar = Calendar(identifier: .iso8601)
        self.timeZone = TimeZone(secondsFromGMT: 0)
    }
    
    /// Default Date Type for Api
    static let iso8601Full: DateFormatter = {
        return DateFormatter(dateFormat: "yyyy-MM-dd HH:mm:ss") => { $0.locale = Locale(identifier: "en_US_POSIX") }
    }()
    static let monthOnly: DateFormatter = {
        return DateFormatter(dateFormat: "MMM")
    }()
    static let dayOnly: DateFormatter = {
        return DateFormatter(dateFormat: "dd")
    }()
    
    static let displayTime: DateFormatter = {
        return DateFormatter(dateFormat: "HH:mm")
    }()
    static let displayDate: DateFormatter = {
        return DateFormatter(dateFormat: "dd.MM.YYYY")
    }()
    
    static let display: DateFormatter = {
        return DateFormatter() => { dateFormatter in
            dateFormatter.dateStyle = .short
            dateFormatter.timeStyle = .short
        }
    }()
}
extension Date {
    /// Default Date Type for Api
    var iso8601Full: String { return DateFormatter.iso8601Full.string(from: self) }
    
    var displayDate: String { return DateFormatter.displayDate.string(from: self) }
    var displayTime: String { return DateFormatter.displayTime.string(from: self) }
    
    var display: String { return displayDate + " " + displayTime}
    var monthOnly: String { return DateFormatter.monthOnly.string(from: self).uppercased() }
    var dayOnly: String { return DateFormatter.dayOnly.string(from: self) }
    
}

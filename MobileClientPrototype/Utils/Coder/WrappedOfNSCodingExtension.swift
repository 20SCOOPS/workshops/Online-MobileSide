//
//  WrappedOfNSCodingExtension.swift
//  ICA
//
//  Created by Dhanu on 21/4/2561 BE.
//  Copyright © 2561 Akarapas Wongkaew. All rights reserved.
//

import Foundation

extension NSCoding {
    var info : WrapperOfNSCoding<Self> { return WrapperOfNSCoding(self) }
}
extension WrapperOfNSCoding {
    var value : Wrapped { return wrapped }
}

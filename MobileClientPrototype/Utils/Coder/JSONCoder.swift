//
//  JSONCoder.swift
//  ICA
//
//  Created by Dhanu on 5/4/2561 BE.
//  Copyright © 2561 Akarapas Wongkaew. All rights reserved.
//

import Foundation
/// JSON object encode/decoder/query items
public class JSONCoder {
    /// Singleton
    public static var shared = JSONCoder()
    public let encoder = JSONEncoder()
    public let decoder = JSONDecoder()
    
    /// Initialize and Setting encoder/decoder
    init() {
        encoder.keyEncodingStrategy = .convertToSnakeCase
        //        encoder.outputFormatting = .prettyPrinted
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        encoder.dateEncodingStrategy = .formatted(.iso8601Full)
        decoder.dateDecodingStrategy = .formatted(.iso8601Full)
    }
    
    /// Encode json object to Data
    ///
    /// - Parameter value: json object
    /// - Returns: Data that encode from value
    /// - Throws: from JSONEncoder.encode()
    public func encode<T>(_ value:T) throws -> Data where T : Encodable {
        if let data = value as? Data { return data }
        return try encoder.encode(value)
    }
    
    /// Decode json object to Data
    ///
    /// - Parameters:
    ///   - type: type of json object
    ///   - data: data of json object
    /// - Returns: json object
    /// - Throws: from JSONDecoder.decode()
    public func decode<T>(_ type: T.Type, from data: Data) throws -> T where T : Decodable {
        if let value = data as? T { return value }
        return try decoder.decode(T.self, from: data)
    }
    
    /// Query Item list from json object
    ///
    /// - Parameter value: Data of json object
    /// - Returns: Query Item
    /// - Throws: from JSONEncoder.encode() and queryItems
    public func queryItems<T>(_ value:T) throws -> [URLQueryItem] where T : Encodable {
        let data = try encode(value)
        return try queryItems(data)
    }
    
    /// Query Item list from data of json object
    ///
    /// - Parameter data: Data of json object
    /// - Returns: Query Item
    /// - Throws: from JSONSerialization.jsonObject()
    public func queryItems(_ data:Data) throws -> [URLQueryItem] {
        let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String:Any] ?? [:]
        return json.compactMap { keyValue -> URLQueryItem? in
            return URLQueryItem(name: keyValue.key, anyValue: keyValue.value)
        }
    }
    
    public func keyValues(_ data:Data) throws -> [String:Any] {
        return try (JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String:Any]) ?? [:]
    }
}

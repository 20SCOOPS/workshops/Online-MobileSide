//
//  CoderExtension.swift
//  ICA
//
//  Created by Dhanu on 21/4/2561 BE.
//  Copyright © 2561 Akarapas Wongkaew. All rights reserved.
//

import Foundation

extension URLQueryItem {
    /// Create Query Item from keyname and dynamic type value
    /// use URLQueryItem with URLComponent to build URL value
    ///
    /// - Parameters:
    ///   - name: keyname
    ///   - anyValue: value with dynamic type
    init?(name: String, anyValue: Any) {
        if let value = (anyValue as? CustomStringConvertible)?.description
            ?? (try? String(json:anyValue)) {
            self.init(name: name, value: value)
        }
        else { return nil }
    }
}

extension String {
    /// Create String from json
    ///
    /// - Parameters:
    ///   - object: input json object
    ///   - stringEncoding: encoding type for convert json
    /// - Throws: invalidData(non-json input) and from JSONSerialization.data(withJSONObject:)
    init(json object:Any, stringEncoding:String.Encoding = .utf8) throws {
        enum EncodingError: Error {
            case invalidData(Any)
        }
        guard JSONSerialization.isValidJSONObject(object) else {
            throw EncodingError.invalidData(object)
        }
        var jsonData:Data
        do{
            jsonData = try JSONSerialization.data(withJSONObject: object)
        } catch let error{
            throw error
        }
        guard let encodedString = String(data: jsonData, encoding: stringEncoding) else {
            throw EncodingError.invalidData(jsonData)
        }
        self.init(encodedString)
    }
    
    /// Create Query String for append after url
    ///
    /// - Parameters:
    ///   - items: Query Items' Array
    ///   - percentEncoded: false if dont want to use percent encode
    init?(withItems items: [URLQueryItem], percentEncoded: Bool = true) {
        var url = URLComponents()
        url.queryItems = items
        guard let queryString = percentEncoded ? url.percentEncodedQuery : url.query else { return nil }
        self.init("?\(queryString)")
    }
}


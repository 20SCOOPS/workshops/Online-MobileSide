import Foundation

extension String {
    var asFileUrl : URL? {
        return URL(fileURLWithPath: self)
    }
    var asUrl : URL? {
        return URL(string: self)
    }
}

extension Data {
    static var defaultPath : URL? {
        return NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true).first?.asFileUrl
    }
    @discardableResult public func save(by key:String)->Promise<URL> {
        guard var path = Data.defaultPath else { return Promise.reject(ErrorMessage(message: "no_default_directory")) }
        path.appendPathComponent(key)
        return Promise { (rs,rj) in
            Dispatcher.background.run {
                do {
                    try self.write(to: path)
                    rs(path)
                }
                catch { rj(error) }
            }
        }
    }
    @discardableResult public static func delete(by key:String)->Promise<URL> {
        guard var path = defaultPath else { return Promise.reject(ErrorMessage(message: "no_default_directory")) }
        path.appendPathComponent(key)
        return Promise { (rs,rj) in
            Dispatcher.background.run {
                do {
                    try FileManager.default.removeItem(at: path)
                    debugPrint("done delete file", path)
                    rs(path)
                }
                catch { rj(error) }
            }
        }
    }
    @discardableResult public static func load(by key:String)->Promise<Data> {
        guard var path = defaultPath else { return Promise.reject(ErrorMessage(message: "no_default_directory")) }
        path.appendPathComponent(key)
        return Promise { (rs,rj) in
            Dispatcher.background.run {
                do {
                    let data = try Data(contentsOf: path)
                    Dispatcher.main.run { rs(data) }
                    debugPrint("done load file", path)
                }
                catch { rj(error) }
            }
        }
    }
    @discardableResult public static func load(by key:String)->Data? {
        guard var path = defaultPath else { return nil }
        path.appendPathComponent(key)
        return try? Data(contentsOf: path)
    }
    
    public func asObject<T:Decodable>(_ type:T.Type) -> T? {
        return try? JSONCoder.shared.decode(T.self, from: self)
    }
}

extension Decodable {
    @discardableResult public static func load(by key:String)->Promise<Self> {
        return Data.load(by: key).then {
            guard let object = $0.asObject(Self.self) else { throw ErrorMessage(message: "cant_decoding") }
            print(object)
            return object
        }
    }
    @discardableResult public static func load(by key:String)->Self? {
        let data:Data? = Data.load(by: key)
        return data?.asObject(Self.self)
    }
}

extension Encodable {
    @discardableResult public func save(by key:String)->Promise<URL> {
        guard let data = asData else { return Promise.reject(ErrorMessage(message: "cant_encoding")) }
        return data.save(by: key)
    }
    public var asData : Data? {
        return try? JSONCoder.shared.encode(self)
    }
}

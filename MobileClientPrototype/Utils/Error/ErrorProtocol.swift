import Foundation

public protocol CodableError : LocalizedError, Codable {}

public extension Promise {
    @discardableResult public func catchs<T:Codable>(_ statusCode:Int, reject:@escaping (T)throws->ReturnType) -> Promise<ReturnType> {
        return catchs(statusCode)  { (data:Data) in
            return try reject( try JSONCoder.shared.decode(T.self, from: data) )
        }
    }
    @discardableResult public func catchs(_ statusCode:Int, reject:@escaping ([String:Any])throws->ReturnType) -> Promise<ReturnType> {
        return catchs(statusCode) { (data:Data) in
            return try reject( try JSONCoder.shared.keyValues(data) )
        }
    }
    @discardableResult public func catchs(_ statusCode:Int, reject:@escaping (Data)throws->ReturnType) -> Promise<ReturnType> {
        return catchs {
            guard let responseError = $0 as? ErrorResponse,
                responseError.statusCode == statusCode else { throw $0 }
            return try reject(responseError.data)
        }
    }
    @discardableResult public func catchs(_ messageName:String, reject:@escaping (String)throws->ReturnType) -> Promise<ReturnType> {
        return catchs {
            guard let error = $0 as? ErrorMessage,
                error.name == messageName else { throw $0 }
            return try reject(error.message)
        }
    }
}

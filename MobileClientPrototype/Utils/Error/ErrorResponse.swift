import Foundation

struct ErrorResponse : CodableError {
    var statusCode : Int
    var data : Data
    var errorData : ErrorData? {
        let errorData = try? JSONCoder.shared.decode(ErrorData.self, from: data)
        if errorData?.message == nil, errorData?.messages == nil {
            return ErrorData(message: String(data:data, encoding:.utf8), messages: nil)
        }
        return errorData
    }
    var errorDescription: String? {
        if let errorData = errorData {
            return "\(statusCode), \(errorData.localizedDescription)"
        }
        if let string = String(data: data, encoding: .utf8) {
            return "\(statusCode), \(string)"
        }
        return nil
    }
}

struct ErrorData: CodableError {
    var message : String?
    var messages : [ErrorMessage]?
    var errorDescription: String?  { return message ?? messages?.description }
}

import Foundation

extension Array : Error where Element : Error {}
extension Error {

    public var statusCode : Int {
        if let e = self as? ErrorResponse { return e.statusCode }
        return 0
    }
    public var asMessage : ErrorMessage {
        if let e = self as? ErrorMessage { return e }
        if let e = self as? ErrorResponse,
            let data = e.errorData { return data.asMessage }
        if let es = self as? [Error],
            let e = es.first {
            return e.asMessage
        }
        return ErrorMessage(message: localizedDescription)
    }
    public var asMessages : [ErrorMessage] {
        if let e = self as? [ErrorMessage] { return e }
        if let e = self as? ErrorMessage { return [e] }
        if let e = self as? ErrorResponse,
            let data = e.errorData { return data.asMessages }
        if let e = self as? ErrorData,
            let messages = e.messages { return messages }
        if let e = self as? [Error] {
            return e.map { return $0.asMessage }
        }
        return [asMessage]
    }
    public func find(name:String="message")->[ErrorMessage] {
        return asMessages.compactMap {
            if $0.name == name { return $0 }
            return nil
        }
    }
    
    public func message(of name:String="message", skipNegative:Bool=false)->String? {
        guard let message = find(name: name).first?.message else { return "" }
        return skipNegative ? nil : message
    }
}

import Foundation

public struct ErrorMessage : CodableError {
    var name : String
    var message : String
    public var errorDescription: String? {
        return message
    }
    init(name:String="message", message:String) {
        self.name=name
        self.message=message
    }
}

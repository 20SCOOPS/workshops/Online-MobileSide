import Foundation

protocol Initializable: class { init() }
private var instances = [String: Initializable]()
protocol Singleton : Initializable {}
extension Singleton {
    static var shared : Self {
        let name = String(describing: Self.self)
        return (instances[name] as? Self) ?? ( Self.init() => {
            instances[name] = $0
            } )
    }
}

infix operator =>
func => <T>(lhs:T, rhs:(inout T)->())->T {
    var pointer = lhs
    rhs(&pointer)
    return pointer
}
